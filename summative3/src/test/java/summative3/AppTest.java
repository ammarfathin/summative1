package summative3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import static org.junit.jupiter.api.Assertions.*;

class AppTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @BeforeEach
    void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @AfterEach
    void restoreStreams() {
        System.setOut(originalOut);
    }

    @Test
    void testValidInput() {
        String[] args = {"1", "2", "3"};
        App.main(args);
        String newline = System.lineSeparator();
        StringBuilder expectedOutput = new StringBuilder();
        expectedOutput.append("1").append(newline);
        expectedOutput.append("2").append(newline);
        expectedOutput.append("3").append(newline);
        assertEquals(expectedOutput.toString(), outContent.toString());
    }

    @Test
    void testEmptyInput() {
        String[] args = {};
        App.main(args);
        assertEquals("", outContent.toString());
    }

    @Test
    void testInvalidInputNonNumeric() {
        String[] args = {"abc"};
        try {
            App.main(args);
            fail("NumberFormatException was expected");
        } catch (NumberFormatException e) {
            assertEquals("For input string: \"abc\"", e.getMessage());
        }
    }

    @Test
    void testNullInput() {
        String[] args = {null};
        try {
            App.main(args);
            fail("NumberFormatException was expected");
        } catch (NumberFormatException e) {
            // Expected exception
            
        }
    }

    @Test
    void testMixedInput() {
        String[] args = {"123abc"};
        try {
            App.main(args);
            fail("NumberFormatException was expected");
        } catch (NumberFormatException e) {
            // Expected exception
        }
    }

    @Test
    void testOverflowInput() {
        String[] args = {"9999999999"};
        try {
            App.main(args);
            fail("NumberFormatException was expected");
        } catch (NumberFormatException e) {
            // Expected exception
        }
    }
}
