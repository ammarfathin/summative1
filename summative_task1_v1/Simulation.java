package summative_task1_v1;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Simulation {
    public static final int DURATION_GREEN = 120;  // 120 seconds
    public static final int DURATION_ORANGE = 10;  // 10 seconds before green/red
    public static final int DURATION_TOTAL = DURATION_GREEN + DURATION_ORANGE;
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public static void main(String[] args) throws IOException {
        TrafficLight[] trafficLights = new TrafficLight[4];
        trafficLights[0] = new TrafficLight("12", LightColor.GREEN);
        trafficLights[1] = new TrafficLight("3", LightColor.RED);
        trafficLights[2] = new TrafficLight("6", LightColor.GREEN);
        trafficLights[3] = new TrafficLight("9", LightColor.RED);

        LocalDateTime startTime = LocalDateTime.now();

        try (FileWriter writer = new FileWriter("traffic-light1.txt")) {
            for (int i = 0; i < DURATION_TOTAL * 4; i++) { // Simulate 4 cycles
                LocalDateTime currentTime = startTime.plusSeconds(i);
                for (int j = 0; j < trafficLights.length; j++) {
                    TrafficLight currentLight = trafficLights[j];
                    updateTrafficLight(currentLight, i % DURATION_TOTAL, j == (i / DURATION_TOTAL) % 4);
                }
                writeTrafficLightsStatus(writer, trafficLights, currentTime);
            }
        }
    }

    private static void updateTrafficLight(TrafficLight trafficLight, int cycleSecond, boolean isActive) {
        if (isActive) {
            if (cycleSecond < DURATION_GREEN - DURATION_ORANGE) {
                trafficLight.setColor(LightColor.GREEN);
            } else {
                trafficLight.setColor(LightColor.ORANGE);
            }
        } else {
            trafficLight.setColor(LightColor.RED);
        }
    }

    private static void writeTrafficLightsStatus(FileWriter writer, TrafficLight[] trafficLights, LocalDateTime currentTime) throws IOException {
        writer.write("Time: " + currentTime.format(formatter) + "\n");
        for (TrafficLight trafficLight : trafficLights) {
            writer.write("Traffic light at " + trafficLight.getPosition() + " is " + trafficLight.getColor() + "\n");
        }
        writer.write("\n");
    }
}
