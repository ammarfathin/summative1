package summative_task1_v1;

public class TrafficLight {
    private String position;
    private LightColor color;

    public TrafficLight(String position, LightColor color) {
        this.position = position;
        this.color = color;
    }

    public String getPosition() {
        return position;
    }

    public LightColor getColor() {
        return color;
    }

    public void setColor(LightColor color) {
        this.color = color;
    }
}
