package summative_task2;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class ParkingApp {
    public static void main(String[] args) {
        ParkingSystem parkingSystem = new ParkingSystem(10); // assuming a parking capacity of 10

        System.out.println("CAR PARKING SIMULATION");
        // Vehicle parked for 30 minutes
        Vehicle vehicle1 = new Vehicle("car", "ABC123", "blue");
        vehicle1.setEntryTime(LocalDateTime.now().minus(30, ChronoUnit.MINUTES));
        int price1 = parkingSystem.calculatePrice(vehicle1);
        System.out.println(vehicle1.getEntryTime());
        System.out.println("Parking for 30 minutes: " + price1);
        System.out.println("Duration "+ vehicle1.getEntryTime().until(LocalDateTime.now(), ChronoUnit.MINUTES));

        Vehicle vehicle = new Vehicle("car", "1HOUR", "blue");
        vehicle.setEntryTime(LocalDateTime.now().minusHours(1));
        int price = parkingSystem.calculatePrice(vehicle);
        System.out.println("Parking for 1 hour: " + price);
        System.out.println("Duration "+ vehicle.getEntryTime().until(LocalDateTime.now(), ChronoUnit.MINUTES));

        // // Vehicle parked for 1 hour and 2 minutes
        Vehicle vehicle2 = new Vehicle("car", "DEF456", "red");
        vehicle2.setEntryTime(LocalDateTime.now().minusMinutes(62));
        int price2 = parkingSystem.calculatePrice(vehicle2);
        System.out.println(vehicle1.getEntryTime());
        System.out.println("Parking for 1 hour and 2 minutes: " + price2);
        System.out.println("Duration "+ vehicle2.getEntryTime().until(LocalDateTime.now(), ChronoUnit.MINUTES));

        // Vehicle parked for 4 hour
        Vehicle vehicle3 = new Vehicle("car", "GHI789", "green");
        vehicle3.setEntryTime(LocalDateTime.now().minusHours(4));
        int price3 = parkingSystem.calculatePrice(vehicle3);
        System.out.println(vehicle1.getEntryTime());
        System.out.println("Parking for 4 hours: " + price3);
        System.out.println("Duration "+ vehicle3.getEntryTime().until(LocalDateTime.now(), ChronoUnit.MINUTES));

        // Vehicle parked for 6 hour 59 minutes
        Vehicle vehicle4 = new Vehicle("car", "XYZ789", "black");
        vehicle4.setEntryTime(LocalDateTime.now().minusHours(6).minusMinutes(59));
        int price4 = parkingSystem.calculatePrice(vehicle4);
        System.out.println("Parking for 6 hours and 59 minutes: " + price4);

        // Vehicle parked for 40 hours
        Vehicle vehicle5 = new Vehicle("car", "JKL012", "white");
        vehicle5.setEntryTime(LocalDateTime.now().minusHours(40));
        int price5 = parkingSystem.calculatePrice(vehicle5);
        System.out.println("Parking for 40 hours: " + price5);
        System.out.println("Duration " + vehicle5.getEntryTime().until(LocalDateTime.now(), ChronoUnit.MINUTES));

        int revenueCar = price1 + price2 + price3 + price4 + price5;
        System.out.println("Total revenue for car parking: " + revenueCar);

        System.out.println("=================================================================="); 
        
        System.out.println("Motorcycle PARKING SIMULATION");
        Vehicle vehicle6 = new Vehicle("motorcycle", "ABC123", "blue");
        vehicle6.setEntryTime(LocalDateTime.now().minusHours(1));
        int price6 = parkingSystem.calculatePrice(vehicle6);
        System.out.println("Parking for 1 hour: " + price6);
        System.out.println("Duration "+ vehicle6.getEntryTime().until(LocalDateTime.now(), ChronoUnit.MINUTES));

        Vehicle vehicle7 = new Vehicle("motorcycle", "1HOUR", "blue");
        vehicle7.setEntryTime(LocalDateTime.now().minusHours(1));
        int price7 = parkingSystem.calculatePrice(vehicle7);
        System.out.println("Parking for 1 hour: " + price7);

        Vehicle vehicle8 = new Vehicle("motorcycle", "DEF456", "red");
        vehicle8.setEntryTime(LocalDateTime.now().minusMinutes(62));
        int price8 = parkingSystem.calculatePrice(vehicle8);
        System.out.println("Parking for 1 hour and 2 minutes: " + price8);

        Vehicle vehicle9 = new Vehicle("motorcycle", "GHI789", "green");
        vehicle9.setEntryTime(LocalDateTime.now().minusHours(4));
        int price9 = parkingSystem.calculatePrice(vehicle9);
        System.out.println("Parking for 4 hours: " + price9);

        Vehicle vehicle10 = new Vehicle("motorcycle", "XYZ789", "black");
        vehicle10.setEntryTime(LocalDateTime.now().minusHours(6).minusMinutes(59));
        int price10 = parkingSystem.calculatePrice(vehicle10);
        System.out.println("Parking for 6 hours and 59 minutes: " + price10);

        Vehicle vehicle11 = new Vehicle("motorcycle", "JKL012", "white");
        vehicle11.setEntryTime(LocalDateTime.now().minusHours(40));
        int price11 = parkingSystem.calculatePrice(vehicle11);
        System.out.println("Parking for 40 hours: " + price11);

        int revenueMotocycle = price6 + price7 + price8 + price9 + price10 + price11;
        System.out.println("Total revenue for motorcycle parking: " + revenueMotocycle);

        System.out.println("==================================================================");
        System.out.println("Total revenue: " + (revenueCar + revenueMotocycle));

    }
}
