package summative_task2;

import java.time.LocalDateTime;
import java.time.Duration;

public class ParkingSystem {
    private final int CAR_FIRST_HOUR_PRICE = 5000;
    private final int CAR_NEXT_HOUR_PRICE = 4500;
    private final int MOTORCYCLE_FIRST_HOUR_PRICE = 4000;
    private final int MOTORCYCLE_NEXT_HOUR_PRICE = 3500;
    private int totalPrice;

    private Vehicle[] parkedVehicles;
    private int numOfParkedVehicles;

    public ParkingSystem(int capacity) {
        parkedVehicles = new Vehicle[capacity];
        numOfParkedVehicles = 0;
    }

    public void parkVehicle(Vehicle vehicle) {
        if (numOfParkedVehicles >= parkedVehicles.length) {
            System.out.println("Parking lot is full");
            return;
        }

        parkedVehicles[numOfParkedVehicles] = vehicle;
        numOfParkedVehicles++;
        System.out.println("Vehicle " + vehicle.getPlatNumber() + " parked");
    }


    public int calculatePrice(Vehicle vehicle) {
        LocalDateTime entryTime = vehicle.getEntryTime();
        if (entryTime == null) {
            System.out.println("Vehicle " + vehicle.getPlatNumber() + " is not parked");
            return 0;
        }
    
        LocalDateTime currentTime = LocalDateTime.now();
        Duration duration = Duration.between(entryTime, currentTime);
        long totalMinutes = duration.toMinutes();
    
        int firstHourPrice;
        int nextHourPrice;

        if (vehicle.getType().equalsIgnoreCase("car")) {
            firstHourPrice = CAR_FIRST_HOUR_PRICE;
            nextHourPrice = CAR_NEXT_HOUR_PRICE;
            if (totalMinutes >= 0 && totalMinutes <= 60) {
                totalPrice = firstHourPrice;
            } else if (totalMinutes > 60) {
                totalPrice = (int) (firstHourPrice + nextHourPrice * ((duration.toMinutes() - 60 )/ 60));
            }
        } else if (vehicle.getType().equalsIgnoreCase("motorcycle")) {
            firstHourPrice = MOTORCYCLE_FIRST_HOUR_PRICE;
            nextHourPrice = MOTORCYCLE_NEXT_HOUR_PRICE;
            if (totalMinutes >= 0 && totalMinutes <= 60) {
                totalPrice = firstHourPrice;
            } else if (totalMinutes > 60) {
                totalPrice = (int) (firstHourPrice + nextHourPrice * ((duration.toMinutes()-60) / 60));
            }
        } else {
            System.out.println("Invalid vehicle type");
            totalPrice = 0;
            
        }
    
        // totalPrice = (int) (firstHourPrice + nextHourPrice * additionalHours);
        return totalPrice;
    }
}
