package summative_task2;

import java.time.LocalDateTime;

public class Vehicle {
    private String type;
    private String platNumber;
    private String color;
    private LocalDateTime entryTime;

    public Vehicle(String type, String platNumber, String color) {
        this.type = type;
        this.platNumber = platNumber;
        this.color = color;
        this.entryTime = null;
    }

    public String getType() {
        return type;
    }

    public String getPlatNumber() {
        return platNumber;
    }

    public String getColor() {
        return color;
    }

    public LocalDateTime getEntryTime() {
        return entryTime;
    }

    public void setEntryTime(LocalDateTime entryTime) {
        this.entryTime = entryTime;
    }

    @Override
    public String toString() {
        return type + " " + platNumber + " " + color;
    }
}