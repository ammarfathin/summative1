package summative_task4;

import java.util.ArrayList;
import java.util.Collections;

public class LiftV2 {
    private int currentFloor;
    private boolean isGoingUp;
    private ArrayList<Integer> destinations;

    public LiftV2() {
        currentFloor = 3;
        isGoingUp = true;
        destinations = new ArrayList<>();
    }

    public void getCurrentFloor() {
        System.out.println("Current floor: " + currentFloor);
    }

    public void addDestination(int floor) {
        if (!destinations.contains(floor)) {
            destinations.add(floor);
            Collections.sort(destinations);
        }
    }

    public void goToNextFloor() {
        if (destinations.isEmpty()) {
            System.out.println("No destinations to go to.");
            return;
        }

        Integer nextFloor = isGoingUp ? getNextUpwardFloor() : getNextDownwardFloor();
        if (nextFloor != null) {
            goToFloor(nextFloor);
        } else {
            isGoingUp = !isGoingUp;
            goToNextFloor();
        }
    }

    private Integer getNextUpwardFloor() {
        for (int floor : destinations) {
            if (floor > currentFloor) return floor;
        }
        return null;
    }

    private Integer getNextDownwardFloor() {
        for (int i = destinations.size() - 1; i >= 0; i--) {
            if (destinations.get(i) < currentFloor) return destinations.get(i);
        }
        return null;
    }

    private void goToFloor(int destinationFloor) {
        if (destinationFloor == currentFloor) {
            System.out.println("Lift is already on floor " + currentFloor);
            return;
        }

        System.out.println("Lift is going " + (destinationFloor > currentFloor ? "up" : "down") + 
                           " from floor " + currentFloor + " to floor " + destinationFloor);
        currentFloor = destinationFloor;
        System.out.println("Current floor: " + currentFloor);
        destinations.remove(Integer.valueOf(destinationFloor));
    }

    public static void main(String[] args) {
        LiftV2 liftv2 = new LiftV2();

        liftv2.addDestination(5);

        liftv2.addDestination(6);

        liftv2.addDestination(3);
        liftv2.addDestination(5);

        // Proses antrian tujuan
        while (!liftv2.destinations.isEmpty()) {
            liftv2.goToNextFloor();
        }

        System.out.println("Semua tujuan telah terlayani.");
    }
}

