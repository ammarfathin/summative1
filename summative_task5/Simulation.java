package summative_task5;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Simulation {
    public static final int DURATION_GREEN = 120;  // 120 seconds
    public static final int DURATION_ORANGE = 10;  // 10 seconds before green/red
    public static final int DURATION_RED = 120;    // 120 seconds for red light
    public static final int EXTRA_GREEN_DURATION = 30; // Extra 30 seconds for jammed lane
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public static void main(String[] args) throws IOException {
        TrafficLight[] trafficLights = new TrafficLight[4];
        for (int i = 0; i < trafficLights.length; i++) {
            trafficLights[i] = new TrafficLight(Integer.toString(i + 1), LightColor.RED);
            trafficLights[i].randomizeVehicleCount(); // Randomly generate vehicle count
        }

        System.out.println("Simulation started at " + LocalDateTime.now().format(formatter));
        System.out.println(trafficLights.length + " traffic lights created");

        LocalDateTime startTime = LocalDateTime.now();

        try (FileWriter writer = new FileWriter("smart-traffic-light2.txt")) {
            for (int i = 0; i < 4; i++) { // Simulate 4 cycles
                // Detect traffic jam and adjust durations
                int[] durations = adjustDurationsBasedOnTraffic(trafficLights);

                LocalDateTime currentTime = startTime.plusSeconds(i * (DURATION_GREEN + DURATION_ORANGE + DURATION_RED));
                for (int j = 0; j < trafficLights.length; j++) {
                    updateTrafficLight(trafficLights[j], durations[j]);
                    writeTrafficLightsStatus(writer, trafficLights, currentTime, j);
                }
            }
        }
    }

    private static int[] adjustDurationsBasedOnTraffic(TrafficLight[] trafficLights) {
        int[] durations = new int[trafficLights.length];
        int maxTraffic = 0;
        int minTraffic = Integer.MAX_VALUE;
        int jammedLane = -1;
        int leastTrafficLane = -1;

        // Find the lane with maximum and minimum traffic
        for (int i = 0; i < trafficLights.length; i++) {
            int traffic = trafficLights[i].getVehicleCount();
            if (traffic > maxTraffic) {
                maxTraffic = traffic;
                jammedLane = i;
            }
            if (traffic < minTraffic) {
                minTraffic = traffic;
                leastTrafficLane = i;
            }
        }

        // Check if the jammed lane has at least 30% more traffic than the other lanes
        if ((maxTraffic - minTraffic) * 100 / minTraffic >= 30) {
            for (int i = 0; i < durations.length; i++) {
                if (i == jammedLane) {
                    durations[i] = DURATION_GREEN + EXTRA_GREEN_DURATION;
                } else if (i == leastTrafficLane) {
                    durations[i] = DURATION_GREEN - EXTRA_GREEN_DURATION;
                } else {
                    durations[i] = DURATION_GREEN;
                }
            }
        } else {
            for (int i = 0; i < durations.length; i++) {
                durations[i] = DURATION_GREEN;
            }
        }
        return durations;
    }
    
    private static void updateTrafficLight(TrafficLight trafficLight, int duration) {
        trafficLight.randomizeVehicleCount(); 
        if (duration == DURATION_GREEN + EXTRA_GREEN_DURATION) {
            trafficLight.setColor(LightColor.GREEN);
        } else if (duration == DURATION_GREEN - EXTRA_GREEN_DURATION) {
            trafficLight.setColor(LightColor.RED);
        } else {
            trafficLight.setColor(LightColor.ORANGE);
        }
    }

    private static void writeTrafficLightsStatus(FileWriter writer, TrafficLight[] trafficLights, LocalDateTime currentTime, int cycle) throws IOException {
        writer.write("Cycle " + (cycle + 1) + ": Time: " + currentTime.format(formatter) + "\n");
        for (TrafficLight trafficLight : trafficLights) {
            writer.write("Traffic light at " + trafficLight.getPosition() + " is " + trafficLight.getColor() + " with " + trafficLight.getVehicleCount() + " vehicles\n");
        }
        writer.write("\n");
    }
}
