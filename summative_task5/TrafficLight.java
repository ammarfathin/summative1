package summative_task5;

public class TrafficLight {
    private String position;
    private LightColor color;
    private int vehicleCount; 

    public TrafficLight(String position, LightColor color) {
        this.position = position;
        this.color = color;
        this.vehicleCount = 0; 
    }

    public String getPosition() {
        return position;
    }

    public LightColor getColor() {
        return color;
    }

    public void setColor(LightColor color) {
        this.color = color;
    }

    public int getVehicleCount() {
        return vehicleCount;
    }

    public void setVehicleCount(int vehicleCount) {
        this.vehicleCount = vehicleCount;
    }

    
    public void randomizeVehicleCount() {
        this.vehicleCount = (int) (Math.random() * 201);
    }
}
